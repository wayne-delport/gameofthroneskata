//
//  ViewController.swift
//  GameOfThronesKata
//
//  Created by Wilmar van Heerden on 2017/08/18.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import UIKit

class ViewController: UITableViewController {

    var characterDetail: GRKCharacterDetailResponse?
    fileprivate let characterCellItemIdentifier = "characterCellItem"

    override func viewDidLoad() {
        super.viewDidLoad()

        configureTableView()

        configureTitle()
    }

    private func configureTableView() {
        tableView.backgroundColor = UIColor.white
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        tableView.allowsSelection = false

        registerCellNib()
    }

    private func registerCellNib() {
        let nib = UINib(nibName: "CharacterTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: characterCellItemIdentifier)
    }

    private func configureTitle() {
        if let characterDetail = self.characterDetail {
            self.title = characterDetail.name
        }
    }
}

extension ViewController {
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "ABOUT THE CHARACTER"
        } else {
            return "SIBLINGS"
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 75.0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: characterCellItemIdentifier, for: indexPath) as? CharacterTableViewCell {
            if let characterDetail = self.characterDetail {
                if indexPath.section == 0 {
                    cell.configure(header: "Title", subheader: characterDetail.title, title: "Status", subtitle: characterDetail.status)
                } else {
                    if let sibling = characterDetail.siblings[indexPath.row] as? GRKSiblings {
                        cell.configure(header: "Name", subheader: sibling.name, title: "Date of birth", subtitle: sibling.dateOfBirth)
                    }
                }
            }

            return cell
        }

        return UITableViewCell()
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            if let characterDetail = self.characterDetail {
                return characterDetail.siblings.count
            } else {
                return 0
            }
        }
    }
}


